/**
*
****************PRACTICA FIN DE CURSO*********************
*---------------------------------------------------------
* Created 20/05/2019
*---------------------------------------------------------
* File: Administrador.h
*---------------------------------------------------------
****************Creacion de Administrador.h*********************
*
*/
#include "persona.h"
#include "cliente.h"
#include <iostream>
#include <string>
#include <stdlib.h>



#ifndef VENDEDOR_H
#define	VENDEDOR_H

using namespace std;


class Administrador : public Usuario{

	protected:
    
	public:
        //Funcionalidad MÃ­nima
        Administrador() : Usuario();	
        ~Administrador();
       
	void insertarComentario(Media *SeleccionadoCatalogo);
	void borrarComentario(Media *SeleccionadoCatalogo);

};
#endif	/* ADMINISTRADOR_H */



