/**
*
****************PRACTICA FIN DE CURSO*********************
*---------------------------------------------------------
* Created 20/05/2019
*---------------------------------------------------------
* File: serie.cpp
*---------------------------------------------------------
****************Creacion de serie.cpp*******************
*
*/

#include <iostream>
#include <stdlib.h>


#include "media.h"
#include "cancion.h"
#include <iomanip>
#include <string.h>

#define restaurar "\033[1;0m"
#define gris "\033[1;30m"
#define rojo "\033[1;31m"
#define verde "\033[1;32m"
#define amarillo "\033[1;33m"
#define azul "\033[1;34m"
#define morado "\033[1;35m"
#define cyan "\033[1;36m"
#define blanco "\033[1;37m"
#define negro "\033[1;38m"

#define Gris "\033[1;40m"
#define Rojo "\033[1;41m"
#define Verde "\033[1;42m"
#define Amarillo "\033[1;43m"
#define Azul "\033[1;44m"
#define Morado "\033[1;45m"
#define Cyan "\033[1;46m"
#define Blanco "\033[1;47m"
#define Negro "\033[1;48m"

using namespace std;


Cancion :: ~Cancion(){

	cout << "Debug : Entra en el Destructor de Cancion. " << endl;  

	this->album = "NULL";    
	this->artista = "NULL";    
	
}
void Cancion::print(){

	this->Media::print();  
 	/*AÑADO LOS ATRIBUTOS PROPIOS DE CANCION*/
	cout << cyan << "ATRIBUTOS PROPIOS DE CANCION : " << restaurar <<  endl;
	cout << cyan << "Artista: " << restaurar << this->artista << endl;
	cout << cyan << "Album: " << restaurar << this->album << endl;
	cout << endl;
}
istream& operator>>(istream &flujo,Cancion *c){
	
	c->Media::pedirParametrosMedia();

	cout << gris << "Dime el Artista : " << restaurar<< endl;
	flujo >> c->artista;
	cout << gris << "Dime el Album  : " << restaurar<< endl;
	flujo >> c->album;
	cout << endl;

}
