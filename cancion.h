/**
*
****************PRACTICA FIN DE CURSO*********************
*---------------------------------------------------------
* Created 20/05/2019
*---------------------------------------------------------
* File: cancion.h
*---------------------------------------------------------
****************Creacion de cancion.h*********************
*
*/
#include <iostream>
#include <string.h>
#include <stdlib.h>

#include "media.h"


#ifndef Cancion_H
#define	Cancion_H

using namespace std;

class Cancion: public Media{

	protected:

		string album;
		string artista;

    
	public:
     	
	//CONSTRUCTORES
	Cancion() : Media(){

		cout << "Debug : Entra en el constructor de Cancion. " << endl;  

		this->album = "NULL";    
		this->artista = "NULL";   
	

	}	
	Cancion(string album,string artista,string tipo,int idMedia,string titulo,string genero,int duracionSegundos,string fechaPublicacion,int pegi,string descripcion,float puntuacion,int totalMeGusta,int totalComentarios):Media(){

		cout << "Debug : Entra en el constructor de Pelicula por parametros. " << endl;  

		this->album = album;    
		this->artista = artista; 
		Media(tipo,idMedia,titulo,genero,duracionSegundos,fechaPublicacion,pegi,descripcion,puntuacion,totalMeGusta,totalComentarios);	
	}
	//DESTRUCTORES
        ~Cancion();

	 /**********************************************************
                ** METODOS SET DE LA CLASE CANCION **
        **********************************************************/   	    
	/* 
         *  @brief metodo que me asigna el artista a la media.
         *  @return No devuelve nada.
         *  @param string artista variable de tipo string que contiene la informacion del artista que le vamos asignar a la media de un usario. 
         *  @post Le asignará una artist al artista de la media de un usuario.
         *  @ver 1.0 
        */
        void setArtista(string artist) { this->artista = artist; }
        /* 
         *  @brief metodo que me asigna una direccion a la media.
         *  @return No devuelve nada.
         *  @param string direccio variable de tipo string que contiene la informacion de la direccion que le vamos asignar a la media de un usario. 
         *  @post Le asignará una direccio a la direccion de la media de un usuario.
         *  @ver 1.0 
        */
        void setAlbum(string albu) { this->album = albu; }
	 /**********************************************************
                ** METODOS GET DE LA CLASE CANCION **
        **********************************************************/   
	/* 
         *  @brief metodo que me devuelve el album de la media un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve el album del reparto de un usuario.
         *  @post se utilizara para los print()/posibles busquedas.
         *  @ver 1.0 
        */
        string getAlbum() { this->album; }
	/* 
         *  @brief metodo que me devuelve el artista de la media un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve la descripcion del artista de un usuario.
         *  @post se utilizara para los print()/posibles busquedas.
         *  @ver 1.0 
        */
        string getArtista() { this->artista; }

	/**********************************************************
                ** METODOS DE LA CLASE Cancion **
        **********************************************************/
	/* 
         *  @brief Me mostrara por pantalla los atributos propios de la cancion + los de la media
         *  @return no devuelve nada.
         *  @post mostrara por pantalla todos los datos de la cancion.
         *  @ver 1.0 
        */
	void print();
	/* 
         *  @brief SOBRECARGA DEL OPERADOR CIN PARA PEDIR los datos de la cancion.
         *  @return no devuelve nada.
         *  @post llamara a su vez al añadir media.
         *  @ver 1.0 
        */
	friend istream& operator>>(istream &flujo,Cancion *c);

};
#endif	/*Cancion_H */



