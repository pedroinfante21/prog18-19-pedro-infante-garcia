/**
*
****************PRACTICA FIN DE CURSO*********************
*---------------------------------------------------------
* Created 20/05/2019
*---------------------------------------------------------
* File: Comentario.cpp
*---------------------------------------------------------
****************Creacion de Comentario.cpp*******************
*
*/
#include <iostream>
#include <string.h>

#include "media.h"
#include "comentario.h"

using namespace std;

Comentario::Comentario(){

	this->idComentario = "NULL";  
	this->login = "NULL";  	
	this->texto = "NULL";
	
	
	cout << "debug: Construyendo el objeto Comentario: " << this->idComentario << " " << this->login << endl; 

}

Comentario::Comentario(string idComentario,string login, string texto){
	

	this->idComentario  = idComentario;  	
	this->login= login;
	this->texto = texto;  
    
	cout << "debug: Construyendo por parámetros el objeto Usuario: " << this->idComentario << " " << this->login << endl; 
    
}

Comentario::~Comentario(){
    
	cout << "debug: Destruyendo el objeto Comentario: " << this->idComentario  << " " << this->login  << endl;
	  
	this->idComentario  = "NULL";  	
	this->login= "NULL";
	this->texto = "NULL";  

}


void Comentario::print(){
  
	cout << endl;
	cout << "Id Comentario : " << this->idComentario << endl;
	cout << "Id Login : " << this->login << endl;
	cout << "Texto : " << this->texto << endl; 
	cout << endl;
  
}


