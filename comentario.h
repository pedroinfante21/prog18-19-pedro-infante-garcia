/**
*
****************PRACTICA FIN DE CURSO*********************
*---------------------------------------------------------
* Created 20/05/2019
*---------------------------------------------------------
* File: comentario.h
*---------------------------------------------------------
****************Creacion de comentario.h********************
*
*/

#include <iostream>
#include <string.h>

#ifndef Comentario_H
#define	Comentario_H

using namespace std;

class Comentario{
    
	protected:

        	string idComentario;
        	string login;
        	string texto;
         	
	public:

        //CONSTRUCTORES
        Comentario();
        //CONSTRUCTORE POR PARAMETROS
        Comentario(string idComentario,string login, string texto);        
        //DESTRUCTOR
        ~Comentario();

        /**********************************************************
                ** METODOS SET DE LA CLASE COMENTARIO **
        **********************************************************/
        /* 
         *  @brief metodo que me asigna un idComentario al elemento de un usuario.
         *  @param string idComentari variable de tipo string que contiene la informacion del idComentario que le vamos asignar al elemento de un usario. 
         *  @return No devuelve nada.
         *  @post Le asignará un idComentari al idComentario del elemento de un usuario.
         *  @ver 1.0 
        */
        void setIdComentario(string idComentari) { this->idComentario = idComentari; }
        /* 
         *  @brief metodo que me asigna un Id a un usuario.
         *  @param string idUsuari variable de tipo string que contiene la informacion del id que le vamos asignar al usuario. 
         *  @return No devuelve nada.
         *  @post Le asignará un id a un usuario.
         *  @ver 1.0 
        */
        void setlogin(string logi){this->login = logi ;}
        /* 
         *  @brief metodo que me asigna un texto al Comentario de un usuario.
         *  @param string text variable de tipo string que contiene la informacion del texto que le vamos asignar al elemento de un usario. 
         *  @return No devuelve nada.
         *  @post Le asignará un text a un texto del comentario de un usuario de un elemento.
         *  @ver 1.0 
        */
        void setTexto(string text) { this->texto = text; }
        
        /**********************************************************
                ** METODOS GET DE LA CLASE COMENTARIO **
        **********************************************************/
        /* 
         *  @brief metodo que me devuelve el id de un Comentario de un Comentario.
         *  @param no tiene parametros.
         *  @return me devuelve el id del Comentario de un elemento.
         *  @post se utilizara para comprobaciones de usuarios  /y para los print().
         *  @ver 1.0 
        */
        string getIdComentario(){this->idComentario;}
        /* 
         *  @brief metodo que me devuelve el idlogin de un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve el id de un usuario.
         *  @post se utilizara para comprobaciones de usuarios  /y para los print().
         *  @ver 1.0 
        */
        string getlogin(){this->login;}
        /* 
         *  @brief metodo que me devuelve el texto de un Comentario.
         *  @param no tiene parametros.
         *  @return me devuelve el texto de un Comentario.
         *  @post se utilizara para los print().
         *  @ver 1.0 
        */
        string getTexto(){this->texto;}

        /**********************************************************
                **METODOS DEL COMENTARIO **
        **********************************************************/
       /* 
         *  @brief Funcion que me imprimira los comentarios de los usuario. 
         *  @param no se le pasa parametros. 
         *  @post devuelve los datos almacenados de un usuario por la pantalla.
         *  @ver 1.0 
        */
        void print();
	
	
		
};
#endif	/* Comentario_H */

