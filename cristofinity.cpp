/**
*
****************PRACTICA FIN DE CURSO*********************
*---------------------------------------------------------
* Created 20/05/2019
*---------------------------------------------------------
* File: cristofinity.cpp
*---------------------------------------------------------
****************Creacion de cristofinity.cpp*******************
*
*/
#include "cristofinity.h"
#include "media.h"
#include "usuario.h"
#include "videojuego.h"
#include "pelicula.h"
#include "cancion.h"
#include "serie.h"
#include "comentario.h"

#include <iostream>
#include <fstream>
#include <typeinfo>
#include <cstdlib>
#include <string>

#define restaurar "\033[1;0m"
#define gris "\033[1;30m"
#define rojo "\033[1;31m"
#define verde "\033[1;32m"
#define amarillo "\033[1;33m"
#define azul "\033[1;34m"
#define morado "\033[1;35m"
#define cyan "\033[1;36m"
#define blanco "\033[1;37m"
#define negro "\033[1;38m"

#define Gris "\033[1;40m"
#define Rojo "\033[1;41m"
#define Verde "\033[1;42m"
#define Amarillo "\033[1;43m"
#define Azul "\033[1;44m"
#define Morado "\033[1;45m"
#define Cyan "\033[1;46m"
#define Blanco "\033[1;47m"
#define Negro "\033[1;48m"

using namespace std;

Cristofinity::Cristofinity(){

	this->totalCatalogo = 0;
	this->totalUsuarios = 0;
	
	this->VectorUsuarios = new Usuario*[0];

	this->Catalogo = new Media*[0];

	cout << "debug: Constructor el objeto CristoFinity: " << this->totalUsuarios << " " << this->totalCatalogo<< endl; 


}
Cristofinity::~Cristofinity(){
	
	for(int i = 0;i < gettotalUsuarios();i++)
		delete this->VectorUsuarios[i];
	

	delete this->VectorUsuarios;
	this->VectorUsuarios = 0;
	
	this->totalUsuarios = 0;

	for(int i = 0;i < gettotalCatalogo();i++)
	
		delete this->Catalogo[i];

		
	delete this->Catalogo;
	this->Catalogo = 0;

	this->totalCatalogo = 0;


	cout << "debug: Destructor el objeto CristoFinity: " << this->totalUsuarios << " " << this->totalCatalogo<< endl; 

	delete this;

}

void Cristofinity::EliminarUsuarioFinity(Usuario** u,int dim,Usuario* eliminar,int posicion){

	u[posicion] = u[dim-1];
	
	delete eliminar;	

}
Usuario** resizeDisminuir(Usuario** u,int &dim){
	
	int Dim_nueva = dim-1;
	Usuario **nueva_dimm= 0;    
	nueva_dimm = new Usuario*[Dim_nueva];

	if (nueva_dimm == 0){       
		cerr << "Error. No hay memoria suficiente. Se abortará la ejecución" << endl;        
		exit(-1);

	}
   
	if (dim > Dim_nueva){        
		dim = Dim_nueva;
	}
   
	for(int i = 0;i < dim;i++){
    
		nueva_dimm[i] = u[i];
    
	}

	delete []u;
	
	dim = Dim_nueva;

	return  nueva_dimm;

}	
Usuario** resizeVectorUsuarios(Usuario** antiguo,int &dim_antigua){
   	
	
	int Dim_nueva = dim_antigua+1;
	
	Usuario **nueva_tabla = 0;    
	nueva_tabla = new Usuario*[Dim_nueva];

	if (nueva_tabla == 0){       
		cerr << "Error. No hay memoria suficiente. Se abortará la ejecución" << endl;        
		exit(-1);

	}

	if (dim_antigua > Dim_nueva){        
		dim_antigua=Dim_nueva;
	}

	for(int i = 0;i < dim_antigua;i++){
    
		nueva_tabla[i] = antiguo[i];
    
	}
	
   	delete []antiguo;

    	dim_antigua = Dim_nueva;

	return nueva_tabla;

}
void Cristofinity::resizeVectorMedia(int dim_antigua){
   	
	int Dim_nueva = dim_antigua;
	
	Media **nueva_tabla = 0;    
	nueva_tabla = new Media*[Dim_nueva];

	if (nueva_tabla == 0){       
		cerr << "Error. No hay memoria suficiente. Se abortará la ejecución" << endl;        
		exit(-1);

	}

	if (dim_antigua > Dim_nueva){        
		dim_antigua=Dim_nueva;
	}

	for(int i = 0;i < Dim_nueva ;i++){
    
		nueva_tabla[i] = this->Catalogo[i];
    
	}
	this->Catalogo = nueva_tabla;
	this->settotalCatalogo(dim_antigua+1);

}
void Cristofinity :: addUsuarioCristoFinity(){

	cout << amarillo << "Introducimos datos del usuario para añadirlo : " << restaurar << endl;
	//1ª Nos declaramos un puntero de tipo Usuario
	Usuario *u;
	//2ª Llamamos al constructor del usuario.
	u = new Usuario();	
	//3º Pedimos al usuario que rellenemos sus datos.
	u->pedirParametrosPersona();
	//4º Aumentamos la dimension del vector.
	this->VectorUsuarios = resizeVectorUsuarios(this->VectorUsuarios,this->totalUsuarios);	
	//5º Incluimos el usuario dentro del vector.
	this->VectorUsuarios[this->totalUsuarios-1] = u;

	cout << cyan << "El usuario se ha añadido correctamente . " << restaurar << endl;
	
}
void Cristofinity :: addMediaCristoFinity(){

	cout << amarillo << "Introducimos datos para añadir Media : " << restaurar << endl;
	//1ª Nos declaramos un puntero de tipo Usuario
	Media *m;
	//2ª Llamamos al constructor del usuario.
	m = new Media();	
	//3º Pedimos al usuario que rellenemos sus datos.
	m->pedirParametrosMedia();
	//4º Aumentamos la dimension del vector.
	this->resizeVectorMedia(this->totalCatalogo);	
	//5º Incluimos el usuario dentro del vector.
	this->Catalogo[this->totalCatalogo-1] = m;

	cout << cyan << "La media se ha añadido correctamente . " << restaurar << endl;
	
}
void Cristofinity::RemoveUserCristoFinity(){

	int posicion = 0;

	if(this->totalUsuarios > 0){
		cout << amarillo << "Dime la posicion del vector que quieres eliminar : " << restaurar << endl;
		cin>>posicion;	
		//Realizo un intercambio de usuarios y hago la eliminacion del usuario que me pide por posicion.	
		this->EliminarUsuarioFinity(this->VectorUsuarios,this->totalUsuarios,this->VectorUsuarios[posicion],posicion);
		//Disminuyo la dimension del vector de los usuarios.
		this->VectorUsuarios = resizeDisminuir(this->VectorUsuarios,this->totalUsuarios);
		cout << rojo << "El Usuario seleccionado ha sido eliminado. " << restaurar << endl;	
	}else{

		cout << rojo << "No hay usuarios en el vector de Usuarios de Cristo Finity " << restaurar << endl;
	}

}
void Cristofinity :: printUserLogged(){

	for (int i = 0; i < this->totalUsuarios;i++)
		this->VectorUsuarios[i]->printUser();

}
void Cristofinity :: printCatalogoLogged(){

	cout << this->totalCatalogo << endl;
	for (int i = 0; i < this->totalCatalogo;i++)
		this->Catalogo[i]->print();

}
void Cristofinity::addNuevaMediaCatalogoXML(Media *m,int cont){

	cout << this->totalCatalogo << endl;
	cout << cont << endl;

	this->totalCatalogo = cont-1;
	cout << "Introduciendo valores al añadir ..." << endl;

	this->resizeVectorMedia(this->totalCatalogo);
	this->Catalogo[this->totalCatalogo-1] = m;	

}
void Cristofinity::ImportarMediaXML(){

	string basura;
	
	ifstream lectura("cristofinity.xml");
	//DECLARACION DE VARIABLES PARA MEDIA
	string tipo = " ";
	string titulo = " ";
	string genero = " ";
	string fechaPublicacion = " ";
	string descripcion = " ";
	int idMedia = 0,duracionSegundos = 0,pegi= 0,totalMeGusta = 0,totalComentarios = 0,totalCatalogo = 0,cont = 1;
	float puntuacion = 0.0;
	int capitulosVacio = 0,ContadorCapitulos;
	//VARIABLES DE SERIES
	int temporadas = 0,capitulos = 0;
	bool estado = false;
	string estado1 = " "; 
	//VARIABLES PELICULAS 
	string reparto = " ",direccion,guion,productor;
	//VARIABLES CANCION 
	string album,artista;
	//VARIABLES VIDEOJUEGOS
	string desarrollador,editor,plataformas;
	int jugadores = 0;
	//VARIABLES DE COMENTARIOS
	int idComentario;
	string login,texto;
	
	
	//PUNTEROS PRINCIPALES 
	Media *m = 0;
	Pelicula *p = 0;
	Serie *s = 0;
	Videojuego *v = 0;
	Cancion * c = 0;
	//Empezamos a IMPORTAR XML
	lectura >> basura;
		lectura>>basura>>totalCatalogo>>basura;
		lectura>>basura>>totalComentarios>>basura;
	lectura >> basura;
	
	while(!lectura.eof()){
		
			lectura >> basura;	
			
				if(basura == "<media>"){			
					
					while(basura != "</media>"){
					
						lectura >> basura;	
							if(basura == "<tipo>"){
							
								lectura >> tipo;
								lectura >> basura;
								
									if(tipo == "Pelicula" ){
							
										while(basura == "</tipo>"){
							
										lectura>>basura;
										if("<idmedia>" == basura)
											lectura >>idMedia>>basura;
								
												while(basura != "</titulo>"){
												lectura >> basura;
													if(basura == "<titulo>"){
														while(basura != "</titulo>"){
															lectura >> basura;						
															titulo+=(basura + " ");
														}
													}
										
												}
												lectura >> basura;
												if(basura == "<genero>")
													lectura  >> genero >> basura;
												lectura >> basura;
												if(basura == "<duracionensegundos>")
													lectura >> duracionSegundos >> basura;
												lectura >> basura;

												if(basura == "<fechapublicacion>")	
												lectura >> fechaPublicacion >> basura;

												lectura >> basura;										
												if(basura == "<pegi>")
												lectura >> pegi >> basura;

												while(basura != "</descripcion>"){
												lectura >> basura;
													if(basura == "<descripcion>"){
														while(basura != "</descripcion>"){
															lectura >> basura;						
															descripcion+=(basura + " ");
														}
													}
												}
												while(basura != "</reparto>"){
												lectura >> basura;
													if(basura == "<reparto>"){
														while(basura != "</reparto>"){
															lectura >> basura;						
															reparto+=(basura + " ");
														}
													}
												}

												while(basura != "</direccion>"){
												lectura >> basura;
													if(basura == "<direccion>"){
														while(basura != "</direccion>"){
															lectura >> basura;						
															direccion+=(basura + " ");
														}
													}

												}
										
												while(basura != "</guion>"){
												lectura >> basura;
													if(basura == "<guion>"){
														while(basura != "</guion>"){
															lectura >> basura;						
															guion+=(basura + " ");
														}
													}

												}
	
												while(basura != "</productor>"){
												lectura >> basura;
													if(basura == "<productor>"){
														while(basura != "</productor>"){
															lectura >> basura;						
															productor+=(basura + " ");
														}
													}

												}
												lectura>> basura;
												if(basura == "<puntuacion>")
												lectura >> puntuacion >> basura;
												lectura>> basura;
												if(basura == "<totalmegusta>")
												lectura >>totalMeGusta >> basura;
												lectura >> basura;
												if(basura == "<totalcomentarios>")
												lectura >> totalComentarios >> basura;
										
				
											//cout << tipo << endl;
											p = new Pelicula();
											p->setTipo(tipo);
											p->setIdMedia(idMedia);
											p->setTitulo(titulo);
											p->setGenero(genero);
											p->setduracionSegundos(duracionSegundos);
											p->setfechaPublicacion(fechaPublicacion);
											p->setpegi(pegi);
											p->setdescripcion(descripcion);
											p->setReparto(reparto);
											p->setProductor(productor);
											p->setGuion(guion);
											p->setDireccion(direccion);
											p->setPuntuacion(puntuacion);
											p->settotalMeGusta(totalMeGusta);
											p->settotalComentarios(totalComentarios);
											
											this->addNuevaMediaCatalogoXML(p,cont);
											cont++;
											if(totalComentarios != 0){
					
												lectura >> basura;	
					
												for(int i = 0; i < totalComentarios;i++){
						
													lectura >> basura;
														lectura >> basura >> idComentario >> basura;	
													//	cout << idComentario << endl;
														lectura >> basura >> login >> basura;
														//cout <<  login << endl;
														lectura >> basura;											
															while(basura != "</texto>"){

																lectura >> basura;						
																texto+=(basura + " ");

															}	

							
														//cout << texto << endl;
														texto = " ";
					
													lectura>>basura;
												}
					
					
											}	
											tipo = " ";
											titulo = " ";
											genero = " ";
											fechaPublicacion = " ";
											descripcion = " ";
											reparto = " ";
											direccion = " ";
											guion = " ";
											productor = " ";
											idMedia = 0;
											duracionSegundos = 0;
											pegi= 0;
											totalMeGusta = 0;
											totalComentarios = 0;
											totalCatalogo = 0;
											puntuacion = 0.0;
										}
										
									}
							
									if(tipo == "Cancion" ){
										lectura >> basura;
										if("<idmedia>" == basura)
											lectura >>idMedia>>basura;
											while(basura != "</titulo>"){
												lectura >> basura;
													if(basura == "<titulo>"){
														while(basura != "</titulo>"){
															lectura >> basura;						
															titulo+=(basura + " ");
														}
														//lectura >> basura;
													}
										
												}
										
												while(basura != "</genero>"){
												lectura >> basura;
													if(basura == "<genero>"){
														while(basura != "</genero>"){
															lectura >> basura;						
															genero+=(basura + " ");
														}
														//lectura >> basura;
													}
										
												}
												lectura >> basura;
												if(basura == "<duracionensegundos>")
													lectura >> duracionSegundos >> basura;
												lectura >> basura;

												if(basura == "<fechapublicacion>")	
												lectura >> fechaPublicacion >> basura;

												lectura >> basura;										
												if(basura == "<pegi>")
												lectura >> pegi >> basura;

												while(basura != "</descripcion>"){
												lectura >> basura;
													if(basura == "<descripcion>"){
														while(basura != "</descripcion>"){
															lectura >> basura;						
															descripcion+=(basura + " ");
														}
													}
												}
												while(basura != "</artista>"){
												lectura >> basura;
														if(basura == "<artista>"){
														while(basura != "</artista>"){
															lectura >> basura;						
															artista+=(basura + " ");
														}
													}
												}
												while(basura != "</album>"){
												lectura >> basura;
														if(basura == "<album>"){
														while(basura != "</album>"){
															lectura >> basura;						
															album+=(basura + " ");
														}
													}

												}
												lectura >> basura;
												if(basura == "<puntuacion>")
												lectura >> puntuacion >> basura;
												lectura>> basura;
												if(basura == "<totalmegusta>")
												lectura >>totalMeGusta >> basura;
												lectura >> basura;
												if(basura == "<totalcomentarios>")
												lectura >> totalComentarios >> basura;

											c = new Cancion();
											c->setTipo(tipo);
											c->setIdMedia(idMedia);
											c->setTitulo(titulo);
											c->setGenero(genero);
											c->setduracionSegundos(duracionSegundos);
											c->setfechaPublicacion(fechaPublicacion);
											c->setpegi(pegi);
											c->setdescripcion(descripcion);
											c->setAlbum(album);
											c->setArtista(artista);
											c->setPuntuacion(puntuacion);
											c->settotalMeGusta(totalMeGusta);
											c->settotalComentarios(totalComentarios);
											
											this->addNuevaMediaCatalogoXML(c,cont);
											cont++;
									
												if(totalComentarios != 0){
													lectura >> basura;	
					
													for(int i = 0; i < totalComentarios;i++){
						
														lectura >> basura;
						
															lectura >> basura >> idComentario >> basura;	
															lectura >> basura >> login >> basura;
															lectura >> basura;											
																while(basura != "</texto>"){

																	lectura >> basura;						
																	texto+=(basura + " ");

																}
															texto = " ";
					
														lectura>>basura;
						
						
													}
												}
											tipo = " ";
											titulo = " ";
											genero = " ";
											fechaPublicacion = " ";
											descripcion = " ";
											artista = " ";
											album = " ";
											idMedia = 0;
											duracionSegundos = 0;
											pegi= 0;
											totalMeGusta = 0;
											totalComentarios = 0;
											totalCatalogo = 0;
											puntuacion = 0.0;	
											basura = " ";	
											}
										
										}
									
										if(tipo == "Serie" ){
											lectura >> basura;
											if("<idmedia>" == basura)
												lectura >>idMedia>>basura;
											
												while(basura != "</titulo>"){
													lectura >> basura;
														if(basura == "<titulo>"){
															while(basura != "</titulo>"){
																lectura >> basura;						
																titulo+=(basura + " ");
															}
														}
										
												}
												lectura >> basura;
												if(basura == "<genero>")
													lectura >> genero >> basura;
												lectura >> basura;
												if(basura == "<duracionensegundos>")
													lectura >> duracionSegundos >> basura;
												lectura >> basura;

												if(basura == "<fechapublicacion>")	
												lectura >> fechaPublicacion >> basura;
								
												lectura >> basura;										
												if(basura == "<pegi>")
												lectura >> pegi >> basura;

								
												while(basura != "</descripcion>"){
												lectura >> basura;
													if(basura == "<descripcion>"){
														while(basura != "</descripcion>"){
															lectura >> basura;						
															descripcion+=(basura + " ");
														}
													}
												}

												while(basura != "</reparto>"){
												lectura >> basura;
													if(basura == "<reparto>"){
														while(basura != "</reparto>"){
															lectura >> basura;						
															reparto+=(basura + " ");
														}
													}
												}		
												while(basura != "</direccion>"){
												lectura >> basura;
													if(basura == "<direccion>"){
														while(basura != "</direccion>"){
															lectura >> basura;						
															direccion+=(basura + " ");
														}
													}

												}	
													
												while(basura != "</guion>"){
												lectura >> basura;
													if(basura == "<guion>"){
														while(basura != "</guion>"){
															lectura >> basura;						
															guion+=(basura + " ");
														}
													}

												}	
												while(basura != "</productor>"){
												lectura >> basura;
													if(basura == "<productor>"){
														while(basura != "</productor>"){
															lectura >> basura;						
															productor+=(basura + " ");
														}
													}
												}
												lectura >> basura;
												if(basura == "<temporadas>")	
												lectura >> temporadas >> basura;
												
												while(basura != "</capitulos>"){
													lectura >> basura;
												}
												lectura >> basura;
												if(basura == "<estado>")
												lectura >> estado1;

												if(estado1 == "finalizada"){
													estado= true;
													lectura >> basura;
												}
												lectura >> basura;
												if(basura == "<puntuacion>")
												lectura >> puntuacion >> basura;
												lectura>> basura;
												if(basura == "<totalmegusta>")
												lectura >>totalMeGusta >> basura;
												lectura >> basura;
												if(basura == "<totalcomentarios>")
												lectura >> totalComentarios >> basura;
				
											s = new Serie();
											s->setTipo(tipo);
											s->setIdMedia(idMedia);
											s->setTitulo(titulo);
											s->setGenero(genero);
											s->setduracionSegundos(duracionSegundos);
											s->setfechaPublicacion(fechaPublicacion);
											s->setpegi(pegi);
											s->setdescripcion(descripcion);
											s->setReparto(reparto);
											s->setGuion(guion);
											s->setTemporadas(temporadas);
											s->setCapitulos(capitulos);
											s->setEstado(estado);
											s->setPuntuacion(puntuacion);
											s->settotalMeGusta(totalMeGusta);
											s->settotalComentarios(totalComentarios);
											
											this->addNuevaMediaCatalogoXML(s,cont);
											cont++;
											if(totalComentarios != 0){
					
												lectura >> basura;	
					
												for(int i = 0; i < totalComentarios;i++){
						
													lectura >> basura;
														lectura >> basura >> idComentario >> basura;	
													//	cout << idComentario << endl;
														lectura >> basura >> login >> basura;
														//cout <<  login << endl;
														lectura >> basura;											
															while(basura != "</texto>"){

																lectura >> basura;						
																texto+=(basura + " ");

															}	

							
														//cout << texto << endl;
														texto = " ";
					
													lectura>>basura;
												}
					
					
											}
											tipo = " ";
											titulo = " ";
											genero = " ";
											fechaPublicacion = " ";
											descripcion = " ";
											reparto = " ";
											direccion = " ";
											guion = " ";
											productor = " ";
											idMedia = 0;
											duracionSegundos = 0;
											pegi= 0;
											totalMeGusta = 0;
											totalComentarios = 0;
											totalCatalogo = 0;
											puntuacion = 0.0;
											//basura = " ";
											}
											if(tipo == "videojuego" ){
											lectura >> basura;
												if("<idmedia>" == basura)
													lectura >>idMedia>>basura;
											
														while(basura != "</titulo>"){
															lectura >> basura;
																if(basura == "<titulo>"){
																	while(basura != "</titulo>"){
																		lectura >> basura;						
																		titulo+=(basura + " ");
																	}
																}
														
														}
														
														lectura >> basura;
														if(basura == "<genero>")
															lectura >> genero >> basura;
														lectura >> basura;
														if(basura == "<duracionensegundos>")
															lectura >> duracionSegundos >> basura;
														lectura >> basura;

														if(basura == "<fechapublicacion>")	
														lectura >> fechaPublicacion >> basura;
								
														lectura >> basura;										
														if(basura == "<pegi>")
														lectura >> pegi >> basura;

								
														while(basura != "</descripcion>"){
														lectura >> basura;
															if(basura == "<descripcion>"){
																while(basura != "</descripcion>"){
																	lectura >> basura;						
																	descripcion+=(basura + " ");
																}
															}
														}

														while(basura != "</desarrollador>"){
														lectura >> basura;
															if(basura == "<desarrollador>"){
																while(basura != "</desarrollador>"){
																	lectura >> basura;						
																	desarrollador+=(basura + " ");
																}
															}
														}		
														while(basura != "</editor>"){
														lectura >> basura;
															if(basura == "<editor>"){
																while(basura != "</editor>"){
																	lectura >> basura;						
																	editor+=(basura + " ");
																}
															}

														}	
														lectura >> basura;
														if(basura == "<jugadores>")	
														lectura >> jugadores >> basura;
														lectura >> basura;
														if(basura == "<puntuacion>")	
														lectura >> puntuacion >> basura;
														lectura>> basura;
														if(basura == "<totalmegusta>")
														lectura >>totalMeGusta >> basura;
														lectura >> basura;
														if(basura == "<totalcomentarios>")
														lectura >> totalComentarios >> basura;
													
													v = new Videojuego();
													v->setTipo(tipo);
													v->setIdMedia(idMedia);
													v->setTitulo(titulo);
													v->setGenero(genero);
													v->setduracionSegundos(duracionSegundos);
													v->setfechaPublicacion(fechaPublicacion);
													v->setpegi(pegi);
													v->setdescripcion(descripcion);
													v->setDesarrollador(desarrollador);
													v->setEditor(editor);
													v->setJugadores(jugadores);
													v->setPuntuacion(puntuacion);
													v->settotalMeGusta(totalMeGusta);
													v->settotalComentarios(totalComentarios);
													
												
									
													this->addNuevaMediaCatalogoXML(v,cont);
													if(totalComentarios != 0){
					
														lectura >> basura;	
					
														for(int i = 0; i < totalComentarios;i++){
						
															lectura >> basura;
						
																lectura >> basura >> idComentario >> basura;	
																//cout << idComentario << endl;
																lectura >> basura >> login >> basura;
															//	cout <<  login << endl;
																lectura >> basura;											
																	while(basura != "</texto>"){

																		lectura >> basura;						
																		texto+=(basura + " ");

																	}	

																texto = " ";
					
															lectura>>basura;
						
						
														}
					
					
													}	
													tipo = " ";
													titulo = " ";
													genero = " ";
													fechaPublicacion = " ";
													descripcion = " ";
													reparto = " ";
													direccion = " ";
													guion = " ";
													productor = " ";
													idMedia = 0;
													duracionSegundos = 0;
													pegi= 0;
													totalMeGusta = 0;
													totalComentarios = 0;
													totalCatalogo = 0;
													puntuacion = 0.0;
											}	
								}						
		
					}
															
		}
}
								
void Cristofinity::ImportarXML(){

	this->ImportarMediaXML();
		
}
void Cristofinity::OpcionesMenuCristoFinity(){

	cout << verde << " [1] Añadir Usuario " << restaurar << endl;
	cout << verde << " [2] Eliminar Usuario " << restaurar << endl;
	cout << verde << " [3] Añadir Media  a catalogo" << restaurar << endl;
	cout << verde << " [4] Eliminar Media del catalogo " << restaurar << endl;
	cout << verde << " [5] Print Usuarios Registrados " << restaurar << endl;
	cout << verde << " [6] Print Catalogo Registradas " << restaurar << endl;
	cout << verde << " [7] Importar XML " << restaurar << endl;
	cout << verde << " [8] Exportar XML " << restaurar << endl;

}
void Cristofinity::conectarConMenuCristoFinity(){

	int n = 0;	

	
	while(9 != n){
		OpcionesMenuCristoFinity();		
		cin>>n; 
         
		if (n >=1 && n<=9){
                       
			switch(n){
                

                        	case 1:
					                                   	
                                	this->addUsuarioCristoFinity();
        
                        	break;
    
                        	case 2:
                	
					this->RemoveUserCristoFinity();
  
                                	
                        	break;  
           	
				case 3:
					
					this->addMediaCristoFinity();			
			
				break; 
				
				case 4:
					
				break;

				case 5:
				
					cout << amarillo << " Print de los usuarios Registrados. " << restaurar << endl;
					this->printUserLogged();	
		
				break;
		
				case 6:
				
					cout << amarillo << " Print de los articulos de Catalogo  Registrados. " << restaurar << endl;
					this->printCatalogoLogged();	
		
				break;
			
				case 7:
				
					cout << amarillo << "Importando XML. " << restaurar << endl;
					this->ImportarXML();
				
		
				break;

				case 8:
				
					
				
		
				break;
				case 9:
				
					cout << " Salida del programa." << endl;	
		
				break;
			
			}
		}

	}
            
}

int main (){
	

	Cristofinity *sistem = 0;
	sistem = new Cristofinity();

	if (system == 0){       
		cerr << "Error. No hay memoria suficiente. Se abortará la ejecución" << endl;        
		exit(-1);

	}

	sistem->conectarConMenuCristoFinity();	

	

}

