/**
*
****************PRACTICA FIN DE CURSO*********************
*---------------------------------------------------------
* Created 20/05/2019
*---------------------------------------------------------
* File: cristofinity.h
*---------------------------------------------------------
****************Creacion de cristofinity.h****************
*
*/

#include <iostream>
#include <string.h>
#include "usuario.h"
#include "media.h"

#ifndef Cristofinity_H
#define	Cristofinity_H

using namespace std;

class Cristofinity{
    
	private:
		
		Usuario **VectorUsuarios;
		int totalUsuarios;
		Media **Catalogo;
		int totalCatalogo;
		
		void resizeVectorMedia(int dim_antigua);
        	
	public:

        //CONSTRUCTORES
        Cristofinity();    
        //DESTRUCTOR
        ~Cristofinity();

        /**********************************************************
                ** METODOS SET DE LA CLASE CRISTOFINITY **
        **********************************************************/           
	/* 
         *  @brief metodo que me asigna un totalUsuarios para el vector de usuarios.
         *  @param int totalUsuario variable de tipo int que contiene la informacion del totalUsuarios para el vector de usuarios. 
         *  @return No devuelve nada.
         *  @post Modificar el valor del totalUsuarios para cuando añada o elimine un usuario.
         *  @ver 1.0 
        */
        void settotalUsuarios(int totalUsuario){this->totalUsuarios = totalUsuario;}
	/* 
         *  @brief metodo que me asigna un totalCatalogo para el vector de usuarios.
         *  @param int totalCatalog variable de tipo int que contiene la informacion del totalCatalogo para el vector de usuarios. 
         *  @return No devuelve nada.
         *  @post Modificar el valor del totalCatalogo para cuando añada o elimine algo del vector de media.
         *  @ver 1.0 
        */
        void settotalCatalogo(int totalCatalog){this->totalCatalogo = totalCatalog;}

        /**********************************************************
                ** METODOS GET DE LA CLASE CRISTOFINITY **
      	**********************************************************/
        /* 
         *  @brief metodo que me devuelve el total de los usuarios que hay en el vector.
         *  @param no tiene parametros.
         *  @return me devuelve la dimension del vector de los usuarios.
         *  @post se utilizara para los print()/resize/filtros.
         *  @ver 1.0 
        */
        int gettotalUsuarios(){this->totalUsuarios;}
        /* 
         *  @brief metodo que me devuelve el total de las medias que hay en el vector.
         *  @param no tiene parametros.
         *  @return me devuelve la dimension del vector.
         *  @post se utilizara para los print()/resize/filtros.
         *  @ver 1.0 
        */
        int gettotalCatalogo(){this->totalCatalogo;}

        /**********************************************************
                **METODOS DE CRISTOFINITY **
        **********************************************************/
       /* 
         *  @brief Funcion que me imprimira los comentarios de los usuario. 
         *  @param no se le pasa parametros. 
         *  @post devuelve los datos almacenados de un usuario por la pantalla.
         *  @ver 1.0 
        */
        void print();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	void ResizeUsuarios();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	void ResizeMedia();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void InsertarUsuario();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	void InsertarMedia();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	void OrdenarUsuariosPorIdUsuario();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void OrdenarPorIdMedia();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	void OrdenarPorIdMediaVisto();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	void OrdenaUsuariosrPorMediaVisto();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	void OrdenarMediaPorPuntuacion();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void OrdenarMediaPorVisualizaciones();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void OrdenarUsuarioPorMediaPendiente();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void BuscarUsuarioPorIdUsuario();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void BuscarMediaPorIdMedia();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void BuscarUsuarioPorPosicion();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void BuscarMediaPorPosicion();
	/* 
         *  @brief Modulo que me conecta con el programa principal-
         *  @param no se le pasa ningun parametro
         *  @post 
         *  @ver 1.0 
        */		
	void conectarConMenuCristoFinity();
	/* 
         *  @brief Modulo que me muestra por pantalla las diferentes partes del programa que vamos a poder ejecutar.
         *  @param 
         *  @post se imprimira por la pantalla un mensaje.
         *  @ver 1.0 
        */		
	void OpcionesMenuCristoFinity();
	/* 
         *  @brief Funcion que me añade un usuario al vector de usuarios de Cristo Finity. 
         *  @param no se le pasan parametros.
         *  @post se aumentara el total de usuarios en +1.
	 *  @post se aumentara una posicion en el vector de Usuarios.
         *  @ver 1.0 
        */	
	void addUsuarioCristoFinity();
	/* 
         *  @brief Funcion que me añade un catalogo al vector de media de Cristo Finity. 
         *  @param no se le pasan parametros.
         *  @post se aumentara el total de productos en el catalogo en +1.
	 *  @post se aumentara una posicion en el vector del Catalogo.
         *  @ver 1.0 
        */
	void addMediaCristoFinity();
	/* 
         *  @brief Eliminamos usuarios de Cristo Finity y filtramos. 
         *  @param no le pasamos parametros.
         *  @post Eliminaremos un usuario de Cristo Finity.
	 *  @post Ejecutaremos un intercambio entre el usuario que quiere eliminar y el de la ultima posicion del vector ocupada.
	 *  @post La dimension del vector disminuira en uno su tamaño. 
         *  @ver 1.0 
        */
	void RemoveUserCristoFinity();
	/* 
         *  @brief  Ejecutamos el intercambio y eliminamos el usuario
         *  @param Usuario** u variable de tipo Usuario** donde contendra todos los usuarios de cristo Finity.
	 *  @param int dim variable de tipo entera que me dira donde se encuentra el ultimo usuario.
	 *  @param Usuario* eliminar variable de tipo Usuario* donde se encuentra la direccion de memoria donde esta el usuario que queremos eliminar.
	 *  @param int posicion variable de tipo entera que me dira donde se encuentra el usuario que queremos eliminar.
         *  @post Eliminamos el usuario.
         *  @ver 1.0 
        */
	void EliminarUsuarioFinity(Usuario** u,int dim,Usuario* eliminar,int posicion);
	/* 
         *  @brief Funcion que me imprime todos los usuarios registrados en Cristo Finity 
         *  @param 
         *  @post mostrar por pantalla todos los usuarios registrados. 
         *  @ver 1.0 
        */
	void printUserLogged();
	/* 
         *  @brief Funcion que me imprime todos las medias del catalogo registrados en Cristo Finity 
         *  @param 
         *  @post mostrar por pantalla todos los medias del catalogo registrados. 
         *  @ver 1.0 
        */
	void printCatalogoLogged();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void MenuUsuario();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void MenuAdministrador();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void Login();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void printUsuaio(Usuario *u);
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void EditarUsuario();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void FusionarUsuario(Usuario *u,Usuario *j);
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void MostrarMediaTotal();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void BorrarPorIdMedia();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void BorrarPorTipoMedia();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void ExportarXML();
	
	/* 
         *  @brief Modulo que me importa todo el catalogo por partes llamando a sus funciones correspondientes.
         *  @param 
         *  @post estara todo el sistema cargado y preparado para ser utilizado.
	 *  @post es lo primero que se ejecutara al inicio del programa para empezar a funcionar con el programa.
         *  @ver 1.0 
        */		
	void ImportarXML();
	/* 
         *  @brief Funcion que me importa solo la media del XML. (videojuegos,series,peliculas y canciones).
         *  @param 
         *  @post el vector polimorfico de la media se encontraran las diferentes medias que han introducido en el xml. 
         *  @ver 1.0 
        */	
	void ImportarMediaXML();
	/* 
         *  @brief Funcion que me importa solo la media del XML. (videojuegos,series,peliculas y canciones).
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	void addNuevaMediaCatalogoXML(Media *m,int cont);

};
#endif	/* CistoFinity_H */
