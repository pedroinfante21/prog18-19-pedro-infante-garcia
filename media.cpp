/**
*
****************PRACTICA FIN DE CURSO*********************
*---------------------------------------------------------
* Created 20/05/2019
*---------------------------------------------------------
* File: media.cpp
*---------------------------------------------------------
****************Creacion de media.cpp*******************
*
*/
 
#include "cristofinity.h"
#include "usuario.h"
#include "media.h"

#include <iostream>
#include <iomanip>
#include <string.h>
#include <fstream>
#include <stdlib.h>
#include <sstream>

#define restaurar "\033[1;0m"
#define gris "\033[1;30m"
#define rojo "\033[1;31m"
#define verde "\033[1;32m"
#define amarillo "\033[1;33m"
#define azul "\033[1;34m"
#define morado "\033[1;35m"
#define cyan "\033[1;36m"
#define blanco "\033[1;37m"
#define negro "\033[1;38m"

#define Gris "\033[1;40m"
#define Rojo "\033[1;41m"
#define Verde "\033[1;42m"
#define Amarillo "\033[1;43m"
#define Azul "\033[1;44m"
#define Morado "\033[1;45m"
#define Cyan "\033[1;46m"
#define Blanco "\033[1;47m"
#define Negro "\033[1;48m"

Media::Media(){

	this->tipo = "NULL";
	this->idMedia = 0;  	
	this->titulo = "NULL";
	this->genero = "NULL";  
	this->duracionSegundos = 0;
	this->fechaPublicacion="NULL";
	this->pegi = 0;
	this->descripcion = "NULL";
	this->puntuacion = 0.0;	
	this->totalMeGusta = 0;
	this->totalComentarios = 0;
	this->vectorComentario = new Comentario*[totalComentarios];
	
	if (vectorComentario == 0){
		cerr << "Error. No hay memoria suficiente. Se abortara la ejecucion" << endl;
        	exit(-1);   
	}

	cout << "debug: Constructor el objeto Media: " << this->idMedia << " " << this->titulo<< endl; 


}
Media::Media(string tipo,int idMedia,string titulo, string genero,int duracionSegundos,string fechaPublicacion,int pegi,string descripcion,float puntuacion,int totalMeGusta,int totalComentarios){

	this->tipo = tipo;
	this->idMedia = idMedia;  	
	this->titulo = titulo;
	this->genero = genero;  
	this->duracionSegundos = duracionSegundos;
	this->fechaPublicacion = fechaPublicacion ;
	this->pegi = pegi;
	this->descripcion = descripcion;
	this->puntuacion = puntuacion;
	this->totalMeGusta = totalMeGusta;
	this->totalComentarios =totalComentarios;

	cout << "debug: Constructor el objeto Media: " << this->idMedia << " " << this->titulo<< endl; 


}
Media :: Media(const Media &m){

	cout << rojo  << "Debug :Constructor por copia "<< restaurar  << endl; 
		
	
	this->tipo = m.tipo;
	this->idMedia = m.idMedia;  	
	this->titulo = m.titulo;
	this->genero = m.genero;  
	this->duracionSegundos = m.duracionSegundos;
	this->fechaPublicacion = m.fechaPublicacion ;
	this->pegi = pegi;
	this->descripcion = descripcion;
	this->puntuacion = puntuacion;
	this->totalMeGusta = totalMeGusta;
	this->totalComentarios =totalComentarios;

	cout << "debug: Constructor el objeto Media: " << this->idMedia << " " << this->titulo<< endl; 

	
}

Media:: ~Media(){
	
	this->tipo = "NULL";
	this->idMedia = 0;  	
	this->titulo = "NULL";
	this->genero = "NULL";  
	this->duracionSegundos = 0;
	this->fechaPublicacion="NULL";
	this->pegi = 0;
	this->descripcion = "NULL";	
	this->puntuacion = 0.0;
	this->totalMeGusta = 0;
	
	
	for(int i = 0;i < this->totalComentarios;i++){
	
		delete this->vectorComentario[i];

	}

	this->totalComentarios = 0;

	delete this->vectorComentario;
	delete this;

	cout << "debug: Destructor el objeto Media: " << this->idMedia << " " << this->titulo<< endl; 

}
istream& operator>>(istream &flujo,Media *m){
	
	cout << gris << "Introduce el tipo : " << restaurar << endl;
	flujo>>m->tipo;
	cout << gris << "Introduce el IdMedia : " << restaurar << endl;
	flujo>>m->idMedia;
	cout << gris << "Dime el titulo: " << restaurar << endl;
	flujo>>m->titulo;
	cout << gris << "Dime la duracion en segundos (INT): "<< restaurar << endl;
	flujo >> m->duracionSegundos;
	cout << gris << "Dime la fecha de la publicacion : " << restaurar<< endl;
	flujo >> m->fechaPublicacion;
	cout << gris << "Dime el pegi (INT): " << restaurar << endl;
	flujo>>m->pegi;
	cout << gris << "Dime la descripcion : "<< restaurar << endl;
	flujo >> m->descripcion;
	cout << gris << "Dime la puntuacion  : " << restaurar<< endl;
	flujo >> m->puntuacion;
	cout << gris << "Dime el total de me gustas : " << restaurar<< endl;
	flujo >> m->totalMeGusta;
	cout << endl;

	return flujo;

}
void Media::print(){
  
	cout << endl;
	cout << amarillo << "El tipo es : " << restaurar << this->tipo << endl;
	cout << amarillo << "El IdMedia : " << restaurar << this->idMedia  << endl;
	cout << amarillo << "El titulo : " << restaurar << this->titulo << endl;
	cout << amarillo << "El Genero : " << restaurar << this->genero << endl;
	cout << amarillo << "La duracion en segundos (INT): "<< restaurar << this->duracionSegundos << endl;
	cout << amarillo << "Dime la fecha de la publicacion : " << restaurar << this->fechaPublicacion<< endl;
	cout << amarillo << "Dime el pegi (INT): " << restaurar << this->pegi << endl;
	cout << amarillo << "Dime la descripcion : " << restaurar<< this->descripcion << endl;
	cout << amarillo << "Dime el total de me gustas : "<< restaurar << this->totalMeGusta << endl;
	cout << amarillo << "Total de comentarios : " << restaurar << this->totalComentarios << endl;	
	cout << amarillo << "La puntuacion es de : " << restaurar << this->puntuacion << endl;
	cout << endl;

	for(int i = 0;i < this->totalComentarios;i++){
		cout << amarillo << " Comentarios de media [ " << i << " ] : " <<  restaurar <<endl;	
		//this->vectorComentario[i];

	} 
	cout << endl;
}  
Media* Media::pedirParametrosMedia(){

	cin>>this;
	
	return this;
}	
