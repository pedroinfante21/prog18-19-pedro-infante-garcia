/**
*
****************PRACTICA FIN DE CURSO*********************
*---------------------------------------------------------
* Created 20/05/2019
*---------------------------------------------------------
* File: Media.h
*---------------------------------------------------------
****************Creacion de Media.h********************
*
*/

#include <iostream>
#include <string.h>

#include "comentario.h"


#ifndef Media_H
#define	Media_H


using namespace std;

class Media{
    
	protected:
		
		string tipo;
        	int idMedia;
        	string titulo;
        	string genero;
		int duracionSegundos;
		string fechaPublicacion;
		int pegi;
		string descripcion;
		//Valoracion **VectorValoraciones;
		//int totalValoraciones;
		float puntuacion;
		int totalMeGusta;		
		Comentario **vectorComentario;
		int totalComentarios;	

		/**
 		 * @brief Cambia la dimensión del vector a una nueva dim_nueva mas pequeña
 		 *  1) Creo un vector nuevo con la nueva dimension
 		 *  2) Copio el contenido del vector que me pasan, en el nuevo vector (Es MÁS CHICO)
 		 *  3) Libero la memoria del vector que me pasan
 		 *  4) Devuelvo el puntero del nuevo vector
 		*@post la dimension del tamño que devuelvo sera una celda mas grande.
 		*/
		 Media** resizeDisminuir(Media** u,int &dim);
		/**
		 * @brief Cambia la dimensión del vector a una nueva dim_nueva
		 *  1) Creo un vector nuevo con la nueva dimension
 	 	 *  2) Copio el contenido del vector que me pasan, en el nuevo vector (OJO SI ES MÁS GRANDE O MÁS CHICO)
		 *  3) Libero la memoria del vector que me pasan
 		 *  4) Devuelvo el puntero del nuevo vector
 		 *@post la dimension del tamño que devuelvo sera una celda mas grande.
 		*/
		Media** resizeVectorUsuarios(Media** antiguo,int &dim_antigua); 	
         	
	public:

        //CONSTRUCTORES
        Media();
        //CONSTRUCTORE POR PARAMETROS
        Media(string tipo,int idMedia,string titulo, string genero,int duracionSegundos,string fechaPublicacion,int pegi,string descripcion,float puntuacion,int totalMeGusta,int totalComentarios);
	//CONSTRUCTOR POR COPIA 
	Media(const Media &m);        
        //DESTRUCTOR
        ~Media();

        /**********************************************************
                ** METODOS SET DE LA CLASE MEDIA **
        **********************************************************/
	/* 
         *  @brief metodo que me asigna un tipo a la media de un usuario.
         *  @param string tip variable de tipo string que contiene la informacion del tipo que le vamos asignar a la media de un usuario. 
         *  @return No devuelve nada.
         *  @post Le asignará un tip al tipo de la media de un usuario.
         *  @ver 1.0 
        */
        void setTipo(string tip) { this->tipo = tip; }
        /* 
         *  @brief metodo que me asigna un idMedia a la media de un usuario.
         *  @param string idMedi variable de tipo string que contiene la informacion del idMedia que le vamos asignar a la media de un usuario. 
         *  @return No devuelve nada.
         *  @post Le asignará un idMedi al idMedia de la media de un usuario.
         *  @ver 1.0 
        */
        void setIdMedia(int idMedi) { this->idMedia = idMedi; }
        /* 
         *  @brief metodo que me asigna un titulo a la media de un usuario.
         *  @param string titulo variable de tipo string que contiene la informacion del titulo que le vamos asignar a la media de un usuario. 
         *  @return No devuelve nada.
         *  @post Le asignará un titulo a la media de un usuario.
         *  @ver 1.0 
        */
        void setTitulo(string titul){this->titulo = titul ;}
        /* 
         *  @brief metodo que me asigna un genero a la media de un usuario.
         *  @param string gener variable de tipo string que contiene la informacion del genero que le vamos asignar a la media de un usario. 
         *  @post Le asignará un gener al genero de la media de un usuario.
         *  @ver 1.0 
        */
        void setGenero(string gener) { this->genero = gener; }
        /* 
         *  @brief metodo que me asigna una duracionSegundos a la media de un usuario.
         *  @return No devuelve nada.
         *  @param int duracionSegundo variable de tipo string que contiene la informacion de la duracionSegundos que le vamos asignar a la media de un usario. 
         *  @post Le asignará una duracionSegundos a la duracionSegundos de la media de un usuario.
         *  @ver 1.0 
        */
        void setduracionSegundos(int duracionSegundo) { this->duracionSegundos = duracionSegundo; }	
	
        /* 
         *  @brief metodo que me asigna una fechaPublicacion a la media de un usuario.
         *  @return No devuelve nada.
         *  @param string fechaPublicacio variable de tipo string que contiene la informacion de la fechaPublicacion que le vamos asignar a la media de un usario. 
         *  @post Le asignará una duracionSegundos a la duracionSegundos de la media de un usuario.
         *  @ver 1.0 
        */
        void setfechaPublicacion(string fechaPublicacio) { this->fechaPublicacion = fechaPublicacio; }
        /* 
         *  @brief metodo que me asigna un pegi a la media de un usuario.
         *  @return No devuelve nada.
         *  @param int peg variable de tipo int que contiene la informacion de la pegi que le vamos asignar a la media de un usario. 
         *  @post Le asignará una peg a la pegi de la media de un usuario.
         *  @ver 1.0 
        */
        void setpegi(int peg) { this->pegi = peg; }
        /* 
         *  @brief metodo que me asigna una descripcion a la media de un usuario.
         *  @return No devuelve nada.
         *  @param int descripcio variable de tipo string que contiene la informacion de la descripcion que le vamos asignar a la media de un usario. 
         *  @post Le asignará una descripcio a la descripcion de la media de un usuario.
         *  @ver 1.0 
        */
        void setdescripcion(string descripcio) { this->descripcion = descripcio; }	    
        /* 
         *  @brief metodo que me asigna un totalComentarios a la media.
         *  @return No devuelve nada.
         *  @param int totalComentario variable de tipo int que contiene la informacion del totalComentarios que le vamos asignar a la media de un usario. 
         *  @post Le asignará una totalVectorDinamicoComentari al totalVectorDinamicoComentario de la media de un usuario.
         *  @ver 1.0 
        */
        void settotalComentario(int totalComentario) { this->totalComentarios = totalComentario; }
	/* 
         *  @brief metodo que me asigna la puntuacion a la media.
         *  @return No devuelve nada.
         *  @param string puntuacio variable de tipo string que contiene la informacion de la puntuacion que le vamos asignar a la media de un usario. 
         *  @post Le asignará una puntuacio a la puntuacion de la media de un usuario.
         *  @ver 1.0 
        */
        void setPuntuacion(float puntuacio) { this->puntuacion = puntuacio; }
        /* 
         *  @brief metodo que me asigna un totalMeGusta a la media.
         *  @return No devuelve nada.
         *  @param int totalMeGust variable de tipo int que contiene la informacion del totalMeGusta que le vamos asignar a la media de un usario. 
         *  @post Le asignará una totalMeGust al totalMeGusta de la media de un usuario.
         *  @ver 1.0 
        */
        void settotalMeGusta(int totalMeGust) { this->totalMeGusta = totalMeGust; }
        /* 
         *  @brief metodo que me asigna un totalComentario a la media.
         *  @return No devuelve nada.
         *  @param int totalComentario variable de tipo int que contiene la informacion del totalComentario que le vamos asignar a la media de un usario. 
         *  @post Le asignará una totalComentario al totalComentarios de la media de un usuario.
         *  @ver 1.0 
        */
        void settotalComentarios(int totalComentario) { this->totalComentarios = totalComentario; }

	
        /**********************************************************
                ** METODOS GET DE LA CLASE MEDIA **
      	**********************************************************
       /* 
         *  @brief metodo que me devuelve el tipo de la media un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve el tipo de la media usuario.
         *  @post se utilizara para los print()/busquedas.
         *  @ver 1.0 
        */
        string getTipo() { this->tipo; }
        /* 
         *  @brief metodo que me devuelve el idMedia de la media un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve el idMedia de la media usuario.
         *  @post se utilizara para los print()/busquedas.
         *  @ver 1.0 
        */
        int getIdMedia() { this->idMedia; }
        /* 
         *  @brief metodo que me devuelve el titulo de la media un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve el titulo de la media usuario.
         *  @post se utilizara para los print()/posibles busquedas.
         *  @ver 1.0 
        */
        string getTitulo(){this->titulo;}
        /* 
         *  @brief metodo que me devuelve el genero de la media un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve el genero de la media usuario.
         *  @post se utilizara para los print()/posibles busquedas.
         *  @ver 1.0 
        */
        string getGenero() { this->genero; }
        /* 
         *  @brief metodo que me devuelve la duracionSegundo de la media un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve la duracionSegundo de la media usuario.
         *  @post se utilizara para los print()/posibles busquedas.
         *  @ver 1.0 
        */
        string getduracionSegundos() { this->duracionSegundos; }
        /* 
         *  @brief metodo que me devuelve la fechaPublicacion de la media un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve la fechaPublicacion de la media usuario.
         *  @post se utilizara para los print()/posibles busquedas.
         *  @ver 1.0 
        */
        string getfechaPublicacion() { this->fechaPublicacion; }
        /* 
         *  @brief metodo que me devuelve la pegi de la media un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve la pegi de la media usuario.
         *  @post se utilizara para los print()/posibles busquedas.
         *  @ver 1.0 
        */
        int getpegi() { this->pegi; }
        /* 
         *  @brief metodo que me devuelve la descripcion de la media un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve la descripcion de la media de un usuario.
         *  @post se utilizara para los print()/posibles busquedas.
         *  @ver 1.0 
        */
        string getdescripcion() { this->descripcion; }	    
	/* 
         *  @brief metodo que me devuelve el productor de la puntuacion un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve la descripcion del productor de un usuario.
         *  @post se utilizara para los print()/posibles busquedas.
         *  @ver 1.0 
        */
	float getPuntuacion() { this->puntuacion; }
 	/* 
         *  @brief metodo que me devuelve el totalMeGusta de la media un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve la totalMeGusta del reparto de un usuario.
         *  @post se utilizara para los print()/posibles busquedas.
         *  @ver 1.0 
        */
        int gettotalMeGusta(int totalMeGust) { this->totalMeGusta; }
        /* 
         *  @brief metodo que me devuelve el totalComentarios de la media un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve el totalComentarios de la media de un usuario.
         *  @post se utilizara para los print()/ bucles/filtros/resizes.
         *  @ver 1.0 
        */
        int gettotalComentarios() { this->totalComentarios; }
	/**********************************************************
                **SOBRECARGAS DE LA CLASE  **
        **********************************************************/

       // operator=(const Usuario &u);
      	friend istream& operator>>(istream &flujo,Media *m);
	friend ostream& operator<<(ostream &flujo,Media *m);


        /**********************************************************
                **METODOS DE LA MEDIA **
        **********************************************************/
       /* 
         *  @brief Funcion que me imprimira las medias de los usuario. 
         *  @param no se le pasa parametros. 
         *  @post devuelve los datos almacenados de una media en el Catalogo por la pantalla.
         *  @ver 1.0 
        */
        virtual void print();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	void ResizeVectorDinamicoComentarios();
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	void InsertarComentario(string comentario);
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */		
	void BorrarComentarioPorIdComentario(string IdComentario);
	/* 
         *  @brief 
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	void MostrarComentarios();
	
	/* 
         *  @brief  Funcion que pedimos los datos de la Media
         *  @param No tiene parametros.
         *  @return Devuelve el puntero de la media.
	 *  @post El puntero de la media contendra los datos del usuario introducido. 
         *  @ver 1.0 
        */
	virtual Media* pedirParametrosMedia();

	Media* addNuevaMediaCatalogoXML(Media *m);

	Media* addMediaXML();

};
#endif	/* Media_H */
