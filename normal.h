/**
*
****************PRACTICA FIN DE CURSO*********************
*---------------------------------------------------------
* Created 20/05/2019
*---------------------------------------------------------
* File: Normal.h
*---------------------------------------------------------
****************Creacion de Normal.h*********************
*
*/
#include "usuario.h"
#include <media.h>
#include <iostream>
#include <string>
#include <stdlib.h>



#ifndef NORMAL_H
#define	NORMAL_H

using namespace std;


class Normal : public Usuario{

	protected:

		Media** ListaMedia;
		int totalListaMedia;  
		void resizeListaMedia(); 

    
	public:
        //CONSTRUCTORES 
        Normal() : Usuario();

	//DESTRUCTOR	
        ~Normal();

	/**********************************************************
                ** METODOS SET DE LA CLASE Usuario Normal **
        **********************************************************/           
	/* 
         *  @brief metodo que me asigna un totalListaMedia para la pelicula.
         *  @param int totalListaMedia variable de tipo int que contiene la informacion del totalListaMedia para el usuario normal. 
         *  @return No devuelve nada.
         *  @post Modificar el valor del reparto para cuando añada una pelicula
         *  @ver 1.0 
        */
	void settotalListaMedia(int totalListaMedi){this->totalListaMedia = totalListaMedi;}
	/* 
         *  @brief metodo que me devuelve un totalListaMedia para la pelicula.
         *  @param no tiene.
         *  @return Me devuelve el total de la lista media.
         *  @post Modificar el valor del reparto para cuando añada una pelicula.
         *  @ver 1.0 
        */
	int gettotalListaMedia(){this->totalListaMedia;}
	
	void buscarListaMediaID();
	void ordenarMediaId();
	void ordenarPuntuacionMedia();       
	void ordenarPorTipo();
	

	void insertarMediaListaMedia(Media *SeleccionadoCatalogo);
	void borrarMediaListaMedia(int idMedia);
	void mostrarListaMedia();
	void insertarComentario();
	void borrarComentario();

};
#endif	/* NORMAL_H */



