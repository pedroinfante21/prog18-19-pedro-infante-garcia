/**
*
****************PRACTICA FIN DE CURSO*********************
*---------------------------------------------------------
* Created 20/05/2019
*---------------------------------------------------------
* File: pelicula.cpp
*---------------------------------------------------------
****************Creacion de pelicula.cpp*******************
*
*/

#include <iostream>
#include <stdlib.h>

#include "pelicula.h"
#include "media.h"
#include <iomanip>
#include <string.h>

#define restaurar "\033[1;0m"
#define gris "\033[1;30m"
#define rojo "\033[1;31m"
#define verde "\033[1;32m"
#define amarillo "\033[1;33m"
#define azul "\033[1;34m"
#define morado "\033[1;35m"
#define cyan "\033[1;36m"
#define blanco "\033[1;37m"
#define negro "\033[1;38m"

#define Gris "\033[1;40m"
#define Rojo "\033[1;41m"
#define Verde "\033[1;42m"
#define Amarillo "\033[1;43m"
#define Azul "\033[1;44m"
#define Morado "\033[1;45m"
#define Cyan "\033[1;46m"
#define Blanco "\033[1;47m"
#define Negro "\033[1;48m"

using namespace std;

Pelicula :: ~Pelicula(){

	cout << "Debug : Entra en el Destructor de Pelicula. " << endl;  

	this->reparto = "NULL";    
	this->direccion = "NULL";    
	this->guion = "NULL";
	this->productor = "NULL"; 
	
}
void Pelicula::print(){

	this->Media::print();  

 	/*AÑADO LOS ATRIBUTOS PROPIOS DE CANCION*/
	cout << cyan << "ATRIBUTOS PROPIOS DE PELICULA : " << restaurar <<  endl;

	cout << cyan << "Reparto: "  << restaurar<< this->reparto << endl;
	cout << cyan << "Direccion: " << restaurar << this->direccion << endl;
	cout << cyan << "Guion: "  << restaurar<< this->guion << endl;
	cout << cyan <<  "Productor : " << restaurar << this->productor << endl;
	cout << endl;

}
istream& operator>>(istream &flujo,Pelicula *p){

	p->Media::pedirParametrosMedia();

	cout << gris << "Dime la reparto : " << restaurar<< endl;
	flujo >> p->reparto;
	cout << gris << "Dime Direccion  : " << restaurar<< endl;
	flujo >> p->direccion;
	cout << gris << "Dime el guion : " << restaurar<< endl;
	flujo >> p->guion;
	cout << gris << "Dime el productor  : " << restaurar<< endl;
	flujo >> p->productor;

}
Pelicula* Pelicula :: pedirParametrosPelicula(){

	cin>> this;
	
	return this;
} 


