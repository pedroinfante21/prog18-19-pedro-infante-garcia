/**
*
****************PRACTICA FIN DE CURSO*********************
*---------------------------------------------------------
* Created 20/05/2019
*---------------------------------------------------------
* File: pelicula.h
*---------------------------------------------------------
****************Creacion de pelicula.h*********************
*
*/
#include <iostream>
#include <string.h>
#include <stdlib.h>

#include "media.h"


#ifndef Pelicula_H
#define	Pelicula_H

using namespace std;

class Pelicula: public Media{

	protected:

		string reparto;
		string direccion;
		string guion;
		string productor;

    
	public:
         //CONSTRUCTORES
        Pelicula(): Media(){

		cout << "Debug : Entra en el constructor de Pelicula. " << endl;  

		this->reparto = "NULL";    
		this->direccion = "NULL";    
		this->guion = "NULL";
		this->productor = "NULL"; 
	

	}	
	Pelicula(string reparto,string direccion,string guion,string productor,string tipo,int idMedia,string titulo,string genero,int duracionSegundos,string fechaPublicacion,int pegi,string descripcion,float puntuacion,int totalMeGusta,int totalComentarios){

	cout << "Debug : Entra en el constructor de Pelicula por parametros. " << endl;  

		this->reparto = reparto;    
		this->direccion = direccion;    
		this->guion = guion;
		this->productor = productor; 
		
		//new Media(tipo,idMedia,titulo,genero,duracionSegundos,fechaPublicacion,pegi,descripcion,puntuacion,totalMeGusta,totalComentarios);
	
	}

	//DESTRUCTORES
        ~Pelicula();
	
        /**********************************************************
                ** METODOS SET DE LA CLASE Pelicula **
        **********************************************************/    
	       
	/* 
         *  @brief metodo que me asigna un reparto para la pelicula.
         *  @param string reparto variable de tipo string que contiene la informacion del reparto para la pelicula. 
         *  @return No devuelve nada.
         *  @post Modificar el valor del reparto para cuando añada una pelicula
         *  @ver 1.0 
        */
        void setReparto(string repart){this->reparto = repart;}
	/* 
         *  @brief metodo que me asigna un direccion para la pelicula.
         *  @param string direccion variable de tipo string que contiene la informacion del direccion para la pelicula. 
         *  @return No devuelve nada.
         *  @post Modificar el valor del direccion para cuando añada una pelicula
         *  @ver 1.0 
        */
        void setDireccion(string direccio){this->direccion = direccio;}
		/* 
         *  @brief metodo que me asigna el guion a la media.
         *  @return No devuelve nada.
         *  @param string guio variable de tipo string que contiene la informacion del guion que le vamos asignar a la media de una pelicula. 
         *  @post Le asignará una guio a la guion de la media de una pelicula.
         *  @ver 1.0 
        */
        void setGuion(string guio) { this->guion = guio; }
	 /* 
         *  @brief metodo que me asigna un productor a la media.
         *  @return No devuelve nada.
         *  @param string producto variable de tipo string que contiene la informacion de la productor que le vamos asignar a la media de un usario. 
         *  @post Le asignará una producto al productor de la media de un usuario.
         *  @ver 1.0 
        */
        void setProductor(string producto) { this->productor = producto; }
	/**********************************************************
                ** METODOS GET DE LA CLASE Pelicula **
        **********************************************************/ 
	    
	/* 
         *  @brief metodo que me devuelve el reparto de la media un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve la descripcion del reparto de un usuario.
         *  @post se utilizara para los print()/posibles busquedas.
         *  @ver 1.0 
        */
        string getReparto() { this->reparto; }
        /* 
         *  @brief metodo que me devuelve el reparto de la media un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve la descripcion del reparto de un usuario.
         *  @post se utilizara para los print()/posibles busquedas.
         *  @ver 1.0 
        */ 

        string getDireccion() { this->direccion; }
		/* 
         *  @brief metodo que me devuelve el guion de la media un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve el guion del reparto de un usuario.
         *  @post se utilizara para los print()/posibles busquedas.
         *  @ver 1.0 
        */
        string getGuion() { this->guion; }
	/* 
         *  @brief metodo que me devuelve el productor de la media un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve la descripcion del productor de un usuario.
         *  @post se utilizara para los print()/posibles busquedas.
         *  @ver 1.0 
        */
        int getProductor() { this->productor; }
	/**********************************************************
                **METODOS DE Pelicula **
        **********************************************************/
	/* 
         *  @brief Me mostrara por pantalla los atributos propios de la pelicula + los de la media
         *  @return no devuelve nada.
         *  @post mostrara por pantalla todos los datos de la pelicula.
         *  @ver 1.0 
        */
	void print();
	/* 
         *  @brief SOBRECARGA DEL OPERADOR CIN PARA PEDIR los datos de la pelicula.
         *  @return no devuelve nada.
         *  @post llamara a su vez al añadir media.
         *  @ver 1.0 
        */
	friend istream& operator>>(istream &flujo,Pelicula *p);
	Pelicula* pedirParametrosPelicula();
	
};
#endif	/* Pelicula_H */



