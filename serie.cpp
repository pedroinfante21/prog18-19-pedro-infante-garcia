/**
*
****************PRACTICA FIN DE CURSO*********************
*---------------------------------------------------------
* Created 20/05/2019
*---------------------------------------------------------
* File: serie.cpp
*---------------------------------------------------------
****************Creacion de serie.cpp*******************
*
*/

#include <iostream>
#include <stdlib.h>


#include "media.h"
#include "serie.h"
#include "pelicula.h"
#include <iomanip>
#include <string.h>

#define restaurar "\033[1;0m"
#define gris "\033[1;30m"
#define rojo "\033[1;31m"
#define verde "\033[1;32m"
#define amarillo "\033[1;33m"
#define azul "\033[1;34m"
#define morado "\033[1;35m"
#define cyan "\033[1;36m"
#define blanco "\033[1;37m"
#define negro "\033[1;38m"

#define Gris "\033[1;40m"
#define Rojo "\033[1;41m"
#define Verde "\033[1;42m"
#define Amarillo "\033[1;43m"
#define Azul "\033[1;44m"
#define Morado "\033[1;45m"
#define Cyan "\033[1;46m"
#define Blanco "\033[1;47m"
#define Negro "\033[1;48m"

using namespace std;

Serie :: ~Serie(){

	cout << "Debug : Entra en el destructor de Serie. " << endl;  
	
	this->temporadas = 0;    
	this->capitulos = 0;    
	this->estado = "NULL";	

}
void Serie::print(){

	this->Media::print();
	cout << endl;
	this->Pelicula::print();
	cout << cyan <<"ATRIBUTOS PROPIOS DE PELICULA : " <<restaurar << endl;
	cout << cyan << "Temporadas : " << restaurar << this->temporadas << endl;
	cout << cyan << "Capitulos : " << restaurar << this->capitulos << endl;
	cout << rojo << "Estado : " << restaurar << this->estado << endl;
	cout << endl;
}
istream& operator>>(istream &flujo,Serie *s){


	s->Media::pedirParametrosMedia();
	s->Pelicula::pedirParametrosPelicula();
	cout << endl;
	cout << "Temporadas : ";
	flujo>>s->temporadas;
	cout << "Capitulos : ";
	flujo>>	s->capitulos;
	cout << "Estado : ";
	flujo>>s->estado;
	cout << endl;

}





