/**
*
****************PRACTICA FIN DE CURSO*********************
*---------------------------------------------------------
* Created 20/05/2019
*---------------------------------------------------------
* File: pelicula.h
*---------------------------------------------------------
****************Creacion de pelicula.h*********************
*
*/
#include <iostream>
#include <string.h>
#include <stdlib.h>

#include "media.h"
#include "pelicula.h"


#ifndef Serie_H
#define	Serie_H

using namespace std;

class Serie : public Pelicula{

	protected:

		int temporadas;
		int capitulos;
		bool estado;
    
	public:
        //CONSTRUCTOR DE SERIE 
	Serie() :Pelicula(){
		cout << "Debug : Entra en el constructor de Serie. " << endl; 
		this->temporadas = 0;    
		this->capitulos = 0;    
		this->estado = "NULL";
	}
	Serie(int temporadas,int capitulos,bool estado,string tipo,int idMedia,string titulo,string genero,int duracionSegundos,string fechaPublicacion,int pegi,string descripcion,float puntuacion,int totalMeGusta,int totalComentarios):Pelicula(){

		cout << "Debug : Entra en el constructor de Series por parametros. " << endl;  
		this->temporadas = temporadas;    
		this->capitulos = capitulos; 
		this->estado = estado;
		//Pelicula(tipo,idMedia,titulo,genero,duracionSegundos,fechaPublicacion,pegi,descripcion,puntuacion,totalMeGusta,totalComentarios);	
	}
	//DESTRUCTOR
	~Serie();

	/**********************************************************
                ** METODOS SET DE LA CLASE SERIE **
        **********************************************************/
	     
        /* 
         *  @brief metodo que me asigna una temporadas a una serie.
         *  @return No devuelve nada.
         *  @param int temporada variable de tipo int que contiene la informacion de la temporadas que le vamos asignar a la serie. 
         *  @post Le asignará una temporada a la temporadas de la serie.
         *  @ver 1.0 
        */
        void setTemporadas(int temporada) { this->temporadas= temporada; }	    
        /* 
         *  @brief metodo que me asigna un capitulo a una serie.
         *  @return No devuelve nada.
         *  @param int capitulo variable de tipo int que contiene la informacion de los capitulos que le vamos asignar a la serie. 
         *  @post Le asignará una temporada a la temporadas de la serie.
         *  @ver 1.0 
        */
        void setCapitulos(int capitulo) { this->capitulos = capitulo; }
        /* 
         *  @brief metodo que me asigna un estado a la media.
         *  @return No devuelve nada.
         *  @param bool estado variable de tipo bool que contiene la informacion del estado que le vamos asignar a la media de un usario. 
         *  @post Le asignará un estad al estado de una serie.
         *  @ver 1.0 
        */
        void setEstado(bool estad) { this->estado = estad; }
	/**********************************************************
                ** METODOS GET DE LA CLASE SERIE **
        **********************************************************/

        /* 
         *  @brief metodo que me devuelve  las temporadas de la serie.
         *  @return devuelve las temporadas de la serie.
         *  @param no tiene.
         *  @post Devolvera el estado de las temporadas por pantalla.
         *  @ver 1.0 
        */
        int getTemporadas() { this->temporadas; }	    
        /* 
         *  @brief metodo que me devuelve  los capitulos de la serie.
         *  @return devuelve los capitulos de la serie.
         *  @param no tiene.
         *  @post Devolvera el estado de una serie por pantalla.
         *  @ver 1.0 
        */
        int getCapitulos(){ this->capitulos; }
        /* 
         *  @brief metodo que me devuelve  un estado de la serie.
         *  @return devuelve el estado de la serie.
         *  @param no tiene.
         *  @post Devolvera el estado de una serie por pantalla.
         *  @ver 1.0 
        */
        bool getEstado() { this->estado; }
	
	/**********************************************************
                ** METODOS DE LA CLASE Serie **
        **********************************************************/
	/* 
         *  @brief Me mostrara por pantalla los atributos propios de la serie + los de la media
         *  @return no devuelve nada.
         *  @post mostrara por pantalla todos los datos de la pelicula.
         *  @ver 1.0 
        */
	void print();
	/* 
         *  @brief SOBRECARGA DEL OPERADOR CIN PARA PEDIR los datos de la serie.
         *  @return no devuelve nada.
         *  @post llamara a su vez al añadir media.
         *  @ver 1.0 
        */
	friend istream& operator>>(istream &flujo,Serie *s);
};
#endif	/* Serie_H */



