/**
*
****************PRACTICA FIN DE CURSO*********************
*---------------------------------------------------------
* Created 20/05/2019
*---------------------------------------------------------
* File: Usuario.cpp
*---------------------------------------------------------
****************Creacion de Usuario.cpp*******************
*
*/
#include <iostream>
#include <string.h>

#include "cristofinity.h"
#include "usuario.h"

#include <iomanip>

#define restaurar "\033[1;0m"
#define gris "\033[1;30m"
#define rojo "\033[1;31m"
#define verde "\033[1;32m"
#define amarillo "\033[1;33m"
#define azul "\033[1;34m"
#define morado "\033[1;35m"
#define cyan "\033[1;36m"
#define blanco "\033[1;37m"
#define negro "\033[1;38m"

#define Gris "\033[1;40m"
#define Rojo "\033[1;41m"
#define Verde "\033[1;42m"
#define Amarillo "\033[1;43m"
#define Azul "\033[1;44m"
#define Morado "\033[1;45m"
#define Cyan "\033[1;46m"
#define Blanco "\033[1;47m"
#define Negro "\033[1;48m"

using namespace std;

Usuario::Usuario(){

	this->tipoUsuario = "NULL";
	this->login = "NULL";  	
	this->nombre = "NULL";
	this->apellido1 = "NULL";  
	this->URLFotoPerfil = "NULL";
	
	this->totalListaMedia = 0;

	this->media_contenido = 0;
	this->media_contenido = new Media *[totalListaMedia];

	if (media_contenido == 0){
		cerr << "Error. No hay memoria suficiente. Se abortara la ejecucion" << endl;
        	exit(-1);   
	}

	cout << "debug: Construyendo el objeto Usuario: " << this->nombre << " " << this->apellido1 << endl; 

}

Usuario::Usuario(string tipoUsuario,string login, string nombre, string apellido1, string URLFotoPerfil,int totalListaMedia){
	
    	this->tipoUsuario = "NULL";
    	this->login = "NULL";  	
	this->nombre = "NULL";
	this->apellido1 = "NULL";  
	this->URLFotoPerfil = "NULL";
    
	this->totalListaMedia = 0;

	Media **media = 0;
	media = new Media *[totalListaMedia];
	
	if (media == 0){
		cerr << "Error. No hay memoria suficiente. Se abortara la ejecucion" << endl;
        	exit(-1);   
	}

	this->media_contenido = media;

	cout << "debug: Construyendo por parámetros el objeto Usuario: " << this->nombre << " " << this->apellido1 << endl;
    
}

Usuario::~Usuario(){
    
	cout << "debug: Destruyendo el objeto Usuario: " << this->nombre << " " << this->apellido1 << endl;
	this->tipoUsuario = "NULL";
	this->login = "NULL";  	
	this->nombre = "NULL";
	this->apellido1 = "NULL";  
	this->URLFotoPerfil = "NULL";
	

	for(int i = 0;i < this->totalListaMedia;i++){
	
		cout << " eliminando archivos media [ " << i << " ] : " << endl;	
		delete this->media_contenido[i];

	}

	this->totalListaMedia = 0;

}


void Usuario::printUser(){
  
	cout << endl;
	cout << "Tipo de User : " << this->tipoUsuario << endl;
	cout << "Id login : " << this->login << endl;
	cout << "Nombre : " << this->nombre << endl;
	cout << "Primer apellido : " << this->apellido1 << endl;
	cout << "URLFotoPerfil : " << this->URLFotoPerfil << endl; 
	cout << "Total Media : " << this->totalListaMedia << endl;

	for(int i = 0;i < this->totalListaMedia;i++){

	} 
	cout << endl;
  
}
/*Usuario& Usuario::operator=(const Usuario &u){

  
	cout << rojo  << "Debug :Operator = "<< restaurar  << endl; 
    
  
	if(&u != this){

  
	}  
	return *this; 
    
}
ostream& operator<<(ostream &flujo,const Usuario &u){
 

  
	return flujo;

}
istream& operator>>(istream &flujo,Usuario &u){

  
  
	cout << cyan << "Vamos a introducirle los datos a un Usuario " << restaurar << endl;

  
	flujo.ignore();

  
	return flujo;

}*/
void ResizeMedia(Media *m);

int BuscarMediaVistoPorIdMedia(string IDmedia);

//Media* SacarMediaVistoPorIdMedia(int posicion);

//void InsertarMediaVisto():

void BorrarMediaVisto(int posicion);

void printMediaPorIdMedia(string IDmedia);

void printMediaPorPosicion(int posicion);	

bool EditarDatosUsuario();

istream& operator>>(istream &flujo,Usuario *u){
	
	cout << gris << "Introduce el tipo de Usuario : " << restaurar << endl;
	flujo>>u->tipoUsuario;
	cout << gris << "Introduce el login : " << restaurar << endl;
	flujo>>u->login;
	cout << gris << "Dime el nombre: " << restaurar << endl;
	flujo>>u->nombre;
	cout << gris << "Primer apellido: "<< restaurar << endl;
	flujo >> u->apellido1;
	cout << gris << "URL Foto Perfil: " << restaurar<< endl;
	flujo >> u->URLFotoPerfil;
	cout << gris << "Total lista media: " << restaurar<< endl;
	flujo >> u->totalListaMedia;
	cout << endl;

	return flujo;

}

Usuario* Usuario::pedirParametrosPersona(){

	cin>>this;

	return this;
}
