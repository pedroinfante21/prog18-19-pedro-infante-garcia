/**
*
****************PRACTICA FIN DE CURSO*********************
*---------------------------------------------------------
* Created 20/05/2019
*---------------------------------------------------------
* File: Usuario.h
*---------------------------------------------------------
****************Creacion de Usuario.h*********************
*
*/
#include <iostream>
#include <string.h>
#include <stdlib.h>

#include "media.h"


#ifndef Usuario_H
#define	Usuario_H

using namespace std;

class Usuario{
    
	protected:
		string tipoUsuario;
        	string login;
        	string nombre;
        	string apellido1;
        	string URLFotoPerfil;
        	Media **media_contenido;
        	int totalListaMedia; 

		/**
 		 * @brief Cambia la dimensión del vector a una nueva dim_nueva mas pequeña
 		 *  1) Creo un vector nuevo con la nueva dimension
 		 *  2) Copio el contenido del vector que me pasan, en el nuevo vector (Es MÁS CHICO)
 		 *  3) Libero la memoria del vector que me pasan
 		 *  4) Devuelvo el puntero del nuevo vector
 		*@post la dimension del tamño que devuelvo sera una celda mas grande.
 		*/
		 Usuario** resizeDisminuir(Usuario** u,int &dim);
		/**
		 * @brief Cambia la dimensión del vector a una nueva dim_nueva
		 *  1) Creo un vector nuevo con la nueva dimension
 	 	 *  2) Copio el contenido del vector que me pasan, en el nuevo vector (OJO SI ES MÁS GRANDE O MÁS CHICO)
		 *  3) Libero la memoria del vector que me pasan
 		 *  4) Devuelvo el puntero del nuevo vector
 		 *@post la dimension del tamño que devuelvo sera una celda mas grande.
 		*/
		Usuario** resizeVectorUsuarios(Usuario** antiguo,int &dim_antigua);  	

	public:

        //CONSTRUCTORES
        Usuario();
        //CONSTRUCTORE POR PARAMETROS
        Usuario(string tipoUsuario,string login,string nombre, string apellido1,string URLFotoPerfil,int totalMedia_contenido);        
        //DESTRUCTOR
        ~Usuario();

        /**********************************************************
                ** METODOS SET DE LA CLASE USUARIO **
        **********************************************************/
        /* 
         *  @brief metodo que me asigna un tipo de usuario.
         *  @param string tipoUsuario variable de tipo string que contiene la informacion del  tipoUsuario que le vamos asignar al usuario. 
         *  @return No devuelve nada.
         *  @post Le asignará un idlogin a un usuario.
         *  @ver 1.0 
        */
        void settipoUsuario(string tipoUsuari){this->tipoUsuario = tipoUsuari ;}
        /* 
         *  @brief metodo que me asigna un Id a un usuario.
         *  @param string logi variable de tipo string que contiene la informacion del id que le vamos asignar al usuario. 
         *  @return No devuelve nada.
         *  @post Le asignará un idlogin a un usuario.
         *  @ver 1.0 
        */
        void setlogin(string logi){this->login = logi ;}
        /* 
         *  @brief metodo que me asigna un nombre a un usuario.
         *  @param string nombre variable de tipo string que contiene la informacion del nombre que le vamos asignar al usuario. 
         *  @return No devuelve nada.
         *  @post Le asignará un nombre a un usuario.
         *  @ver 1.0 
        */
        void setNombre(string nombr) { this->nombre = nombr; }
        /* 
         *  @brief metodo que me asigna un apellido1 a un usuario.
         *  @param string ape1 variable de tipo string que contiene la informacion del apellido1 que le vamos asignar al usuario. 
         *  @return No devuelve nada.
         *  @post Le asignará un apellido1 a un usuario.
         *  @ver 1.0 
        */

        void setApellido1(string ape1){this->apellido1 = ape1 ;}
        /* 
         *  @brief metodo que me asigna un URLFotoPerfil a un usuario.
         *  @param string URLFotoPerfi variable de tipo string que contiene la informacion del URLFotoPerfil que le vamos asignar al usuario. 
         *  @return No devuelve nada.
         *  @post Le asignará un URLFotoPerfil a un usuario.
         *  @ver 1.0 
        */
        void setURLFotoPerfil(string URLFotoPerfi){this->URLFotoPerfil = URLFotoPerfi;}
        /* 
         *  @brief metodo que me asigna un totalListaMedia a un usuario.
         *  @param int totalListaMedi variable de tipo int que contiene la informacion del totalListaMedia que le vamos asignar al usuario. 
         *  @return No devuelve nada.
         *  @post Le asignará un totalListaMedia a un usuario.
         *  @ver 1.0 
        */
        void settotalListaMedia(int totalListaMedi){this->totalListaMedia = totalListaMedi;}
   
        /**********************************************************
                ** METODOS GET DE LA CLASE USUARIO **
        **********************************************************/
        /* 
         *  @brief metodo que me devuelve el tipoUsuario de un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve el tipoUsuario de un usuario.
         *  @post se utilizara para comprobaciones de usuarios  /y para los print().
         *  @ver 1.0 
        */
        string gettipoUsuario(){this->tipoUsuario;}
        /* 
         *  @brief metodo que me devuelve el idlogin de un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve el idlogin de un usuario.
         *  @post se utilizara para comprobaciones de usuarios  /y para los print().
         *  @ver 1.0 
        */
        string getlogin(){this->login;}
        /* 
         *  @brief metodo que me devuelve el nombre de un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve el nombre de un usuario.
         *  @post se utilizara para los print().
         *  @ver 1.0 
        */
    
        string getNombre(){this->nombre; }
        /* 
         *  @brief metodo que me devuelve el apellido1 de un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve el apellido1 de un usuario.
         *  @post se utilizara para los print().
         *  @ver 1.0 
        */

        string getApellido1(){this->apellido1;}
        /* 
         *  @brief metodo que me devuelve el URLFotoPerfil de un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve el URLFotoPerfil de un usuario.
         *  @post se utilizara para los print().
         *  @ver 1.0 
        */
        string getURLFotoPerfil(){this->URLFotoPerfil;}
        /* 
         *  @brief metodo que me devuelve el totalMedia_contenido de un usuario.
         *  @param no tiene parametros.
         *  @return me devuelve el totalMedia_contenido de un usuario.
         *  @post se utilizara para los print()/filtros/bucles.
         *  @ver 1.0 
        */
        int gettotalListaMedia(){this->totalListaMedia;}
        /**********************************************************
                **SOBRECARGAS DE LA CLASE  **
        **********************************************************/

       // operator=(const Usuario &u);
      	friend istream& operator>>(istream &flujo,Usuario *u);
	friend ostream& operator<<(ostream &flujo,Usuario *u);


        /**********************************************************
                **METODOS DEL USUARIO **
        **********************************************************/
       /* 
         *  @brief Funcion que me imprimira los datos del usuario. 
         *  @param no se le pasa parametros. 
         *  @post devuelve los datos almacenados de un usuario por la pantalla.
         *  @ver 1.0 
        */
        void printUser();
	/* 
         *  @brief  
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	void ResizeMedia(Media *m);
	/* 
         *  @brief  
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	int BuscarMediaVistoPorIdMedia(string IDmedia);
	/* 
         *  @brief  
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	void SacarMediaVistoPorIdMedia(int posicion);
	/* 
         *  @brief  
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	//void InsertarMediaVisto():
	/* 
         *  @brief  
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	void BorrarMediaVisto(int posicion);
	/* 
         *  @brief  
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	void printMediaPorIdMedia(string IDmedia);
	/* 
         *  @brief  
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	void printMediaPorPosicion(int posicion);	
	/* 
         *  @brief  
         *  @param 
         *  @post 
         *  @ver 1.0 
        */
	bool EditarDatosUsuario();
	/* 
         *  @brief  Funcion que pedimos los datos de un Usuario
         *  @param No tiene parametros.
         *  @return Devuelve el puntero del Usuario.
	 *  @post El puntero del Usuario contendra los datos del usuario introducido. 
         *  @ver 1.0 
        */
	Usuario* pedirParametrosPersona();
		
};
#endif	/* usuario_H */	
