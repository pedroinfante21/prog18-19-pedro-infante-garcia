/**
*
****************PRACTICA FIN DE CURSO*********************
*---------------------------------------------------------
* Created 20/05/2019
*---------------------------------------------------------
* File: videojuego.cpp
*---------------------------------------------------------
****************Creacion de videojuegos.cpp*******************
*
*/

#include <iostream>
#include <stdlib.h>


#include "media.h"
#include "videojuego.h"
#include <iomanip>
#include <string.h>

#define restaurar "\033[1;0m"
#define gris "\033[1;30m"
#define rojo "\033[1;31m"
#define verde "\033[1;32m"
#define amarillo "\033[1;33m"
#define azul "\033[1;34m"
#define morado "\033[1;35m"
#define cyan "\033[1;36m"
#define blanco "\033[1;37m"
#define negro "\033[1;38m"

#define Gris "\033[1;40m"
#define Rojo "\033[1;41m"
#define Verde "\033[1;42m"
#define Amarillo "\033[1;43m"
#define Azul "\033[1;44m"
#define Morado "\033[1;45m"
#define Cyan "\033[1;46m"
#define Blanco "\033[1;47m"
#define Negro "\033[1;48m"

using namespace std;


Videojuego :: ~Videojuego(){
		
	cout << "Entra en Destructor de videojuegos " << endl;
	this->desarrollador = "NULL";
	this->editor = "NULL";
	this->plataformas = "NULL";
	this->jugadores = 0;
		
}

void Videojuego::print(){

	this->Media::print();
	cout << endl; 
	cout << cyan << "ATRIBUTOS PROPIOS DE Videojuego : " << restaurar <<  endl;

	cout << cyan << "Desarrollador : " << restaurar<< this->desarrollador << endl;
	cout << cyan << "Editor : " << restaurar<< this->editor << endl;
	cout << cyan << "Plataformas : " << restaurar<< this->plataformas << endl;
	cout << cyan << "Jugadores : " << restaurar << this->jugadores << endl;

}

istream& operator>>(istream &flujo,Videojuego *v){


	v->Media::pedirParametrosMedia();
	cout <<"Desarrollador : ";
	flujo>>v->desarrollador;
	cout << "Editor : "; 
	flujo>>v->editor;
	cout << "Plataformas : ";
	flujo>> v->plataformas;
	cout << "Jugadores : "; 
	flujo>>v->jugadores;
	cout << endl;

}





