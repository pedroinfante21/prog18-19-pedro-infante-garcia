/**
*
****************PRACTICA FIN DE CURSO*********************
*---------------------------------------------------------
* Created 20/05/2019
*---------------------------------------------------------
* File: Videojuego.h
*---------------------------------------------------------
****************Creacion de Videojuego.h*********************
*
*/
#include <iostream>
#include <string.h>
#include <stdlib.h>

#include "media.h"


#ifndef Videojuego_H
#define	Videojuego_H

using namespace std;

class Videojuego: public Media{

	protected:

		string desarrollador;
		string editor;
		string plataformas;
		int jugadores;

	public:
        //CONSTRUCTOR VIDEOJUEGOS
        Videojuego() : Media(){
		cout << "Entra en el Constructor de videojuegos " << endl;
		this->desarrollador = "NULL";
		this->editor = "NULL";
		this->plataformas = "NULL";
		this->jugadores = 0;
		
	}
	Videojuego(string desarrollador,string editor,string plataformas,int jugadores,string tipo,int idMedia,string titulo,string genero,int duracionSegundos,string fechaPublicacion,int pegi,string descripcion,float puntuacion,int totalMeGusta,int totalComentarios):Media(){

		cout << "Debug : Entra en el constructor de Videojuegos por parametros. " << endl;  
		this->desarrollador = desarrollador;    
		this->editor = editor; 
		this->plataformas = plataformas;
		Media(tipo,idMedia,titulo,genero,duracionSegundos,fechaPublicacion,pegi,descripcion,puntuacion,totalMeGusta,totalComentarios);	
	}
	//DESTRUCTOR VIDEOJUEGOS	
        ~Videojuego();
	
	   /**********************************************************
                ** METODOS SET DE LA CLASE VIDEOJUEGOS **
        **********************************************************/
	     
        /* 
         *  @brief metodo que me asigna el desarrollador de un videojuego.
         *  @return No devuelve nada.
         *  @param string desarrolado variable de tipo string que contiene la informacion del desarrolador que le vamos asignar al videojuego. 
         *  @post Le asignará un desarrollado a la desarrollador del videojuego.
         *  @ver 1.0 
        */
        void setDesarrollador(string desarrollado) { this->desarrollador = desarrollado; }	    
        /* 
         *  @brief metodo que me asigna el editor de un videojuego.
         *  @return No devuelve nada.
         *  @param string editor variable de tipo string que contiene la informacion del editor que le vamos asignar al videojuego. 
         *  @post Le asignará un edito al editor del videojuego.
         *  @ver 1.0 
        */
        void setEditor(string edito) { this->editor = edito; }
        /* 
         *  @brief metodo que me asigna una plataformas a la media.
         *  @return No devuelve nada.
         *  @param string plataforma variable de tipo string que contiene la informacion del plataformas que le vamos al videojuego. 
         *  @post Le asignará un plataforma a las plataformas de una serie.
         *  @ver 1.0 
        */
        void setPlataformas(string plataforma) { this->plataformas = plataforma; }
        /* 
         *  @brief metodo que me asigna unos jugadores a la media.
         *  @return No devuelve nada.
         *  @param int jugadore variable de tipo int que contiene la informacion de los jugadores que le vamos al videojuego. 
         *  @post Le asignará un jugadore a los jugadores de un videojuego.
         *  @ver 1.0 
        */
        void setJugadores(int jugadore) { this->jugadores = jugadore; }
	/**********************************************************
                ** METODOS GET DE LA CLASE VIDEOJUEGOS **
        **********************************************************/
        /* 
         *  @brief metodo que me devuelve el desarrollador de los videojuegos.
         *  @return Se mostrara por pantalla el desarrollador que juegan al videojuego.
         *  @param no tiene.
         *  @post Me mostrara por pantalla el desarrollador que juegan al videojuego..
         *  @ver 1.0 
        */
        string getDesarrollador() { this->desarrollador; }	    
        /* 
         *  @brief metodo que me devuelve el editor de los videojuegos.
         *  @return Se mostrara por pantalla el editor que juegan al videojuego.
         *  @param no tiene.
         *  @post Me mostrara por pantalla el editor que juegan al videojuego..
         *  @ver 1.0 
        */
        string getEditor() { this->editor; }
        /* 
         *  @brief metodo que me devuelve las plataformas de los videojuegos.
         *  @return Se mostrara por pantalla las plataformas que juegan al videojuego.
         *  @param no tiene.
         *  @post Me mostrara por pantalla las plataformas que juegan al videojuego..
         *  @ver 1.0 
        */
        string getPlataformas() { this->plataformas; }
        /* 
         *  @brief metodo que me devuelve unos jugadores al videojuego.
         *  @return Se mostrara por pantalla los jugadores que juegan al videojuego.
         *  @param no tiene.
         *  @post Me mostrara por pantalla los jugadores que juegan al videojuego..
         *  @ver 1.0 
        */
        int getJugadores() { this->jugadores;}

	/**********************************************************
                ** METODOS DE LA CLASE VIDEOJUEGOS **
        **********************************************************/
	/* 
         *  @brief Me mostrara por pantalla los atributos propios de los videojuegos + los de la media
         *  @return no devuelve nada.
         *  @post mostrara por pantalla todos los datos de los videojuego.
         *  @ver 1.0 
        */
	void print();
	/* 
         *  @brief SOBRECARGA DEL OPERADOR CIN PARA PEDIR los datos de los videojuegos.
         *  @return no devuelve nada.
         *  @post llamara a su vez al añadir media.
         *  @ver 1.0 
        */
	friend istream& operator>>(istream &flujo,Videojuego *v);
};
#endif	/* Videojuego_H */



